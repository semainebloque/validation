package net.tncy.lfr.validator;



public class InventoryEntry {

     Book book;
     int quantity;
     float averagePrice;
     float currentPrice;

    public InventoryEntry(Book book, int quantity, float averagePrice, float currentPrice) {
        this.book = book;
        this.quantity = quantity;
        this.averagePrice = averagePrice;
        this.currentPrice = currentPrice;
    }

    public Book getBook() {
        return book;
    }

    public int getQuantity() {
        return quantity;
    }

    public float getAveragePrice() {
        return averagePrice;
    }

    public float getCurrentPrice() {
        return currentPrice;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setAveragePrice(float averagePrice) {
        this.averagePrice = averagePrice;
    }

    public void setCurrentPrice(float currentPrice) {
        this.currentPrice = currentPrice;
    }
}
